package com.curso.ejercicio.garaje.service;

/**
 * 
 * Interfaz conducible: interfaz que se emplea para aportar los métodos
 * requeridos a toda clase hija de la clase vehiculo
 * 
 * @version 1.0
 * @author Santiago Velasco
 *
 */
public interface Conducible {

	/**
	 * Inicio del trayecto.
	 * 
	 * Para poder usar cualquiera de los vehiculos primero es necesario montarse y
	 * conducirlo (en el caso de los vehiculos a motor arrancarlo) Las variables de
	 * tiempo y distancia se inicializan en 0.
	 */
	public void conducir();

	/**
	 * Avance del trayecto.
	 * 
	 * Para poder avanzar en el trayecto una vez iniciado se debe registrar la
	 * distancia a recorrer.
	 * 
	 * @param distancia recorrida en Km
	 */
	public void avanzar(double distancia);

	/**
	 * Dar marcha atras.
	 * 
	 * Durante el trayecto se da la opcion de dar marcha atras, parecido al avance
	 * pero en este caso la distancia disminuye ya que se va en direccion contraria.
	 * 
	 * @param distancia recorrida en Km
	 */
	public void retroceder(double distancia);

	/**
	 * Finalizar trayecto.
	 * 
	 * Sin no se para del todo el vehiculo no se puede finalizar el trayecto.
	 * 
	 * Este metodo devuelve el tiempo y la distancia total recorrida durante el
	 * viaje a modo de estadistica.
	 */
	public void parar();

}