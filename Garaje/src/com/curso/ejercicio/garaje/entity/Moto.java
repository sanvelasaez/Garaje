package com.curso.ejercicio.garaje.entity;

/**
 * Clase Moto heredada de VehiculoMotor.
 * 
 * Instanciar la clase Moto para ser usada
 * 
 * @version 1.0
 * @author Santiago Velasco
 *
 */
public class Moto extends VehiculoMotor {

	/**
	 * Constructor de la clase Moto
	 * 
	 * @param color del vehiculo
	 * @param matricula del vehiculo
	 */
	public Moto(String color, String matricula) {
		super(tipoVehiculo.MOTO, color, matricula);
	}

}
