package com.curso.ejercicio.garaje.entity;

/**
 * Clase Bici heredada de Vehiculo.
 * 
 * Instanciar la clase Bici para ser usada
 * 
 * @version 1.0
 * @author Santiago Velasco
 *
 */
public class Bici extends Vehiculo {

	/**
	 * Constructor de la clase Bici
	 * 
	 * @param color del vehiculo
	 */
	public Bici(String color) {
		super(tipoVehiculo.BICI, color);
	}

}
