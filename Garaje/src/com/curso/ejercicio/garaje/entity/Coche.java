package com.curso.ejercicio.garaje.entity;

/**
 * Clase Coche heredada de VehiculoMotor.
 * 
 * Instanciar la clase Coche para ser usada
 * 
 * @version 1.0
 * @author Santiago Velasco
 *
 */
public class Coche extends VehiculoMotor {

	/**
	 * Constructor de la clase Coche
	 * 
	 * @param color del vehiculo
	 * @param matricula del vehiculo
	 */
	public Coche(String matricula, String color) {
		super(tipoVehiculo.COCHE, color, matricula);
	}

}
