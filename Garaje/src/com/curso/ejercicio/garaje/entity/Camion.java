package com.curso.ejercicio.garaje.entity;

import java.util.ArrayList;

/**
 * Clase Camion heredada de VehiculoMotor.
 * 
 * Instanciar la clase Camion para ser usada
 * 
 * @version 1.1 Se ha agregado el atributo tacometro para registrar las
 *          velocidades
 * @author Santiago Velasco
 *
 */
public class Camion extends VehiculoMotor {

	private ArrayList<Double> tacometro;// Array para guardar todas las velocidades

	/**
	 * Constructor de la clase Camion
	 * 
	 * @param color     del vehiculo
	 * @param matricula del vehiculo
	 */
	public Camion(String color, String matricula) {
		super(tipoVehiculo.CAMION, color, matricula);
		this.tacometro = new ArrayList<Double>();
	}

	/**
	 * 
	 * @param velocidad en Km/h
	 */
	public void addVelocidad(Double velocidad) {
		tacometro.add(velocidad);
	}

	/**
	 * 
	 * @return velocidades en cadena
	 */
	public String getVelocidades() {
		String velocidades = "";
		for (Double vel : tacometro) {
			velocidades += vel + "  ";
		}
		return velocidades;
	}

	/**
	 * 
	 * @return tacometro
	 */
	public ArrayList<Double> getTacometro() {
		return tacometro;
	}

	@Override
	public String dameEstado() {
		return super.dameEstado() + "\nVelocidades del tacometro: " + getVelocidades();
	}

}
